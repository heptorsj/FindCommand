/*
  Comando Find de Unix Implementado con funciones del lenguaje C
  stat,mod, etc...
  El Comando debe ser capaz de:
  - Encontrar archivos a traves de rutas absolutas y relativas [20 puntos] - listo
  - La busqueda deberán tener el caracter de recursivas [20 puntos] - listo
  - Busquedas por atributos:
    * Nombre de archivo [10 puntos] - listo
    * Tipo de archivo [10 puntos] - listo
    * Por dueño [10 puntos] - listo
    * Por permisos [10 puntos]  - listo
  - Podran ejecutarse comandos adicionales sobre archivos
  usando un nuevo proceso y exec [20 puntos] - pendiente


*/
//-----------------------------------------------------------
/*
  Sintaxis:
  myfind [ruta] [parametros]
*/
#include <string.h>
#include <stdio.h>
#include <sys/stat.h> // Para al estructura
#include <sys/types.h> // Tipos de stat
#include <fcntl.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/wait.h>
/*  Banderas
   flags[0] => -name
   flags[1] => -iname
   flags[2] => -type
   flags[3] => -exec
   flags[4] => -perm
   flags[5] => -user


*/
int flags[6] = {0,0,0,0,0,0};
// Las banderas estan apagadas
char type;
char name[25];
char iname[25];
char execpapu[25];
char permisos[5];
char duenio[25];
char *resultados[50000];
char *argv2[50];
int contador = 0;
int argumentos = 0;
int e;
int valid = 0;
int elementos = 0;
// Strings a manejar
/* Funciones del programa */

// get de mode
int getChmod(const char *path,char cmod[5]){
    struct stat ret;
    char mods[5] = {'0','0','0','0'};
    if (stat(path, &ret) == -1) {
        return -1;
    }
    int sticky = ret.st_mode & S_ISVTX;
    int setuser = ret.st_mode & S_ISUID;
    int setgroup = ret.st_mode & S_ISGID;
    int mod = ((ret.st_mode & S_IRUSR)|(ret.st_mode & S_IWUSR)|(ret.st_mode & S_IXUSR)|/*owner*/
        (ret.st_mode & S_IRGRP)|(ret.st_mode & S_IWGRP)|(ret.st_mode & S_IXGRP)|/*group*/
        (ret.st_mode & S_IROTH)|(ret.st_mode & S_IWOTH)|(ret.st_mode & S_IXOTH));
    if(sticky){
      mods[0] = '1';
    }
    if(setuser){
      mods[0] = '4';
    }
    if(setgroup){
      mods[0] = '2';
    }
    else{
      mods[0] = '0';

    }
    int i;
    for(i=1;i < 4;i++)
    {
      mods[i] = (char) (mod / 100) + '0';
      int temp = (mod % 100) * 10;
      mod = temp;
    }
    mods[5] = '\0';
    strcpy(cmod,mods);

}

void readVariable(char **argv){
int i = 2;
  while(argv[i] != NULL){
    if(strcmp(argv[i],"-type") == 0){
      flags[2] = 1;
      type =  argv[i+1][0];
      printf("Buscando tipos: %c\n",type);
    }
    if(strcmp(argv[i],"-name") == 0){
      flags[0] = 1;
      strcpy(name,argv[i+1]);
      printf("Buscando Archivos Que Coincidan Con: %s\n",name);

    }
    if(strcmp(argv[i],"-iname") == 0){
       flags[1] = 1;
       strcpy(iname,argv[i+1]);
       printf("Buscando nombre: %s\n",iname);

    }
    if(strcmp(argv[i],"-exec") == 0){
       flags[3] = 1;
       strcpy(execpapu,argv[i+1]);
       char commands[50];
       int j = i + 2;
       int k = 0;
       if(argv[j] != NULL){
         argumentos = 1;
         strcpy(commands,argv[j]);
         argv2[k] = malloc(strlen(execpapu)+1);
         strcpy(argv2[k],execpapu);
         argv2[1] = malloc(50000);
         k=2;
       while(strcmp(argv[j],"+") != 0 ){
         //printf("%s\n",argv[j]);
         strcpy(commands,argv[j]);
         argv2[k] = malloc(strlen(commands));
         strcpy(argv2[k],commands);
         printf("%s\n",argv2[k]);
         j++;
         k++;
       }
       elementos = k+1;
       }
       printf("Ejecutando: %s\n",execpapu);

    }
    if(strcmp(argv[i],"-perm") == 0) {
      printf("Buscando Archivos Con Permiso: %s \n",argv[i+1]);
      strcpy((permisos),argv[i+1]);
      flags[4] = 1;
    }
    if(strcmp(argv[i],"-user") == 0){
      printf("Busncado archivos propiedad de: %s\n",argv[i+1]);
      strcpy(duenio,argv[i+1]);
      flags[5] = 1;
    }
    i++;
  }
}
void filtroDuenio(char *filtro[5000],int c){
  int i;
  struct stat archivo;
  struct passwd  *pwd; 
  for(i=0; i < c;i++){
    stat(filtro[i],&archivo);
     pwd = getpwuid(archivo.st_uid);
      if( strcmp(pwd->pw_name,duenio) == 0 ){
             if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],filtro[i]);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,filtro[i],(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",filtro[i]);
                }
     }

  }

}
void filtroPermisos(char *filtro[50000],int c){
  int i = 0;
  struct stat archivo;
  char *resultados2[50000];
  int contador2 = 0;
  for(i=0; i < c;i++){
    stat(filtro[i],&archivo);
    char mode[5];
    getChmod(filtro[i],mode);
    if(strcmp(mode,permisos) == 0){
      if(flags[5] == 0){
        if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],filtro[i]);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,filtro[i],(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",filtro[i]);
                }
      }else{
        resultados2[contador2] = malloc(1024);
        strcpy(resultados2[contador2],filtro[i]);
        contador2++;
      }
    }
  }
  if( flags[5] == 1 ){
    filtroDuenio(resultados2,contador2);
  }
}

void filtrarResultados(char *filtro[50000]){
  int i = 0;
  struct stat archivo;
  char *resultados2[50000];
  int contador2 = 0;
  for(i=0; i < contador; i ++) {
    lstat(filtro[i],&archivo);
    switch (type) {
      case 'b': // Para bloque
       if(S_ISBLK(archivo.st_mode)){
         if( flags[4] == 0 && flags[5] == 0){
           if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],filtro[i]);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,filtro[i],(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",filtro[i]);
                }
         }else{
           resultados2[contador2] = malloc(1024);
           strcpy(resultados2[contador2],filtro[i]);
           contador2++;
         }
       }
      break;
      case 'c': // Para caracter
      if(S_ISCHR(archivo.st_mode)){
        if( flags[4] == 0 && flags[5] == 0){
          if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],filtro[i]);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,filtro[i],(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",filtro[i]);
                }
        }else{
          resultados2[contador2] = malloc(1024);
          strcpy(resultados2[contador2],filtro[i]);
          contador2++;
        }

       }
      break;
      case 'd': // Para directorio
      if(S_ISDIR(archivo.st_mode)){
        if( flags[4] == 0 && flags[5] == 0){
          printf("%s\n",filtro[i]);
        }else{
          resultados2[contador2] = malloc(1024);
          strcpy(resultados2[contador2],filtro[i]);
          contador2++;
        }

       }
      break;
      case 'p': // Para fifo
      if(S_ISFIFO(archivo.st_mode)){
        if( flags[4] == 0 && flags[5] == 0){
          if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],filtro[i]);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,filtro[i],(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",filtro[i]);
                }
        }else{
          resultados2[contador2] = malloc(1024);
          strcpy(resultados2[contador2],filtro[i]);
          contador2++;
        }

       }
      break;
      case 'f': // Para regular
      if(S_ISREG(archivo.st_mode)){
        if( flags[4] == 0 && flags[5] == 0){
          if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],filtro[i]);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,filtro[i],(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",filtro[i]);
                }
        }else{
          resultados2[contador2] = malloc(1024);
          strcpy(resultados2[contador2],filtro[i]);
          contador2++;
        }

       }
      break;
      case 'l': // Para liga suave
      if(S_ISLNK(archivo.st_mode) ){
        if( flags[4] == 0 && flags[5] == 0){
          if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],filtro[i]);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,filtro[i],(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",filtro[i]);
                }
        }else{
          resultados2[contador2] = malloc(1024);
          strcpy(resultados2[contador2],filtro[i]);
          contador2++;
        }
       }
      break;
      case 's': // Para socket
      if(S_ISSOCK(archivo.st_mode)){
        if( flags[4] == 0 && flags[5] == 0){
          if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],filtro[i]);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,filtro[i],(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",filtro[i]);
                }
        }else{
          resultados2[contador2] = malloc(1024);
          strcpy(resultados2[contador2],filtro[i]);
          contador2++;
        }

       }
      break;
      default:
      printf("Tipo de archivo invalido \n ");
      printf("Opciones validas: [b],[c],[d],[p],[f],[l],[s]\n");
      break;
     }

  }
  if(flags[4] == 1 ){
    // Filtrar tambien por permisos
    filtroPermisos(resultados2,contador2);


  }else{
    if( flags[5] == 1){
    // Filtrar tambien por duenio
    filtroDuenio(resultados2,contador2);
    }
  }

}
int OpenDir(char * directorio,int flag){
  DIR *actual; // Directorio actual
  struct dirent *entrada; // Donde se guardaran los datos del directorio
  struct stat archivo; // Donde se guardaran los datos de cada archivo
  struct stat archivo2;
  struct passwd  *pwd; // para el propietario del archivo
  if((actual = opendir(directorio)) == NULL ){
    //perror("Directorio invalido");

  }else{
    //printf("Buscando en: %s\n",directorio);
    while((entrada = readdir(actual)) != NULL){
        char path[1024] ="";
        char mode[5];
        if( strcmp("/",directorio) == 0){
                  strcat(path,entrada->d_name);
         }else{
                  strcat(path,directorio);
                  strcat(path,"/");
                  strcat(path,entrada->d_name);
        }
        stat(path,&archivo);
        getChmod(path,mode);
        if((S_ISDIR(archivo.st_mode))){
           if(strcmp(entrada->d_name,".") == 0 || strcmp(entrada->d_name,"..") == 0){
              //printf("Ignorando %s\n",entrada->d_name);
           }else{
             if(flag == 0) {
                char ruta2[1204] = "";
                strcat(ruta2,directorio);
                if( strcmp("/",directorio) == 0){
                  strcat(ruta2,entrada->d_name);
                }else{
                  strcat(ruta2,"/");
                  strcat(ruta2,entrada->d_name);
                }
                //printf("---------%s--------\n",ruta2);
                if(type == 'd'){
                    if(flags[0] == 0 && flags[4] == 0 && flags[5] == 0){
                      printf("%s\n",ruta2);
                      if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],path);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,path,NULL);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }

                    }
                    else{
                      if(flags[0] == 1){
                         if(strcmp(entrada->d_name,name) == 0){
                           if(flags[4] == 1){
                             char mode[5];
                             getChmod(ruta2,mode);
                             if(strcmp(mode,permisos) == 0){
                              if(flags[5] == 1){
                                struct passwd *pwd2;
                                pwd2 = getpwuid(archivo.st_uid);
                                if(strcmp(pwd2->pw_name,duenio) == 0){
                                  printf("%s\n",ruta2);
                                }

                              }else{
                                printf("%s\n",ruta2);
                              }
                             }

                           }
                           if(flags[5] == 1){

                           }
                           if(flags[4] == 0 && flags[5] == 0){
                             printf("%s\n",ruta2);
                           }
                         }

                      }else{
                        if(flags[4] == 1){
                          char mode[5];
                          getChmod(ruta2,mode);
                          if(strcmp(mode,permisos) == 0){
                            if(flags[5] == 1) {
                             struct passwd *pwd2;
                             pwd2 = getpwuid(archivo.st_uid);
                             if(strcmp(pwd2->pw_name,duenio) == 0){
                               printf("%s\n",ruta2);
                             }
                            }else{
                              printf("%s\n",ruta2);
                            }
                          }
                        }else{
                          if(flags[5] == 1){
                            struct passwd *pwd2;
                            pwd2 = getpwuid(archivo.st_uid);
                            if(strcmp(pwd2->pw_name,duenio) == 0){
                              printf("%s\n",ruta2);
                            }

                          }else{
                            printf("%s\n",ruta2);
                          }
                        }
                      }
                    }
                }
                OpenDir(ruta2,0);

             }else{
               char cwd[1024]="";
               //getcwd(cwd, sizeof(cwd));
               //printf("%s\n",cwd);
               strcat(cwd,directorio);
               strcat(cwd,"/");
               strcat(cwd,entrada->d_name);
               //printf("%s \n",cwd);
               if(type == 'd'){
                    if(flags[0] == 0 && flags[3] == 0 && flags[4] == 0 && flags[5] == 0){
                      printf("%s\n",cwd);
                    }
                    else{
                      if(flags[0] == 1){
                         if(strcmp(entrada->d_name,name) == 0){
                           if(flags[4] == 1){
                             char mode[5];
                             getChmod(cwd,mode);
                             if(strcmp(mode,permisos) == 0){
                              if(flags[5] == 1){
                                struct passwd *pwd2;
                                pwd2 = getpwuid(archivo.st_uid);
                                if(strcmp(pwd2->pw_name,duenio) == 0){
                                  printf("%s\n",cwd);
                                }

                              }else{
                                printf("%s\n",cwd);
                              }
                             }

                           }
                           if(flags[5] == 1){

                           }
                           if(flags[4] == 0 && flags[5] == 0){
                             printf("%s\n",cwd);
                           }
                         }

                      }else{
                        if(flags[4] == 1){
                          char mode[5];
                          getChmod(cwd,mode);
                          if(strcmp(mode,permisos) == 0){
                            if(flags[5] == 1) {
                             struct passwd *pwd2;
                             pwd2 = getpwuid(archivo.st_uid);
                             if(strcmp(pwd2->pw_name,duenio) == 0){
                               printf("%s\n",cwd);
                             }
                            }else{
                              printf("%s\n",cwd);
                            }
                          }
                        }else{
                          if(flags[5] == 1){
                            struct passwd *pwd2;
                            pwd2 = getpwuid(archivo.st_uid);
                            if(strcmp(pwd2->pw_name,duenio) == 0){
                              printf("%s\n",cwd);
                            }

                          }else{
                            printf("%s\n",cwd);
                          }
                        }
                      }
                    }
                }
               OpenDir(cwd,1);

             }
           }
        }else{
          // En esta parte se realiza la busqueda por achivos
          // Primero buscar por nombre, en caso de que la bandera de nombre este activada
         if(flags[0] == 1){
           if(strcmp(entrada->d_name,name) == 0){
             if(flags[2] == 0  && flags[4] == 0 && flags[5] == 0) {
                if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],path);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,path,NULL);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",path);
                }
             }else{
               // Si hay otras opciones se deben filtrar los archivos ya encontrados
                 resultados[contador] = malloc(1024);
                 strcpy(resultados[contador],path);
                 contador++;
             }
           }
         }
         else{
           if(flags[2] == 1 ){ // Busqueda  por tipos
             int  k = 0;
             lstat(path,&archivo2);
             if(type != 'b' && type != 'c' && type != 'd'  && type != 's' && type != 'l' && type != 'f' && type != 'p'   ){
               printf("Error Tipo Invalido\n");
               break;
             }
             switch (type) {
              case 'b': // Para bloque
               if(S_ISBLK(archivo2.st_mode)){
                if(flags[4] == 1 || flags[5] == 1){
                  // habra que filtrarResultados
                  resultados[contador] = malloc(1024);
                  strcpy(resultados[contador],path);
                  contador++;
                }else{
                  if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],path);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,path,(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",path);
                }

               }
             break;
             case 'c': // Para caracter
             if(S_ISCHR(archivo2.st_mode)){
               if(flags[4] == 1 || flags[5] == 1){
                 // habra que filtrarResultados
                 resultados[contador] = malloc(1024);
                 strcpy(resultados[contador],path);
                 contador++;
               }else{
                 if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],path);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,path,(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",path);
                }

              }

             }
            break;
           case 'd': // Para directorio
            if(S_ISDIR(archivo2.st_mode)){
              if(flags[4] == 1 || flags[5] == 1){
                // habra que filtrarResultados
                resultados[contador] = malloc(1024);
                strcpy(resultados[contador],path);
                contador++;
              }else{
                printf("%s\n",path);

             }

           }
           break;
          case 'p': // Para fifo
           if(S_ISFIFO(archivo2.st_mode)){
             if(flags[4] == 1 || flags[5] == 1){
               // habra que filtrarResultados
               resultados[contador] = malloc(1024);
               strcpy(resultados[contador],path);
               contador++;
             }else{
               if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],path);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,path,(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",path);
                }

            }

          }
          break;
         case 'f': // Para regular
          if(S_ISREG(archivo2.st_mode)){
            if(flags[4] == 1 || flags[5] == 1){
              // habra que filtrarResultados
              resultados[contador] = malloc(1024);
              strcpy(resultados[contador],path);
              contador++;
            }else{
              if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],path);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,path,(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",path);
                }


           }

           }
          break;
         case 'l': // Para liga suave
         if(S_ISLNK(archivo2.st_mode)){
           if(flags[4] == 1 || flags[5] == 1){
             // habra que filtrarResultados
             resultados[contador] = malloc(1024);
             strcpy(resultados[contador],path);
             contador++;
           }else{
             if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],path);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,path,(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",path);
                }

          }

         }
         break;
         case 's': // Para socket
          if(S_ISSOCK(archivo2.st_mode)){
            if(flags[4] == 1 || flags[5] == 1){
              // habra que filtrarResultados
              resultados[contador] = malloc(1024);
              strcpy(resultados[contador],path);
              contador++;
            }else{
              if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],path);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,path,(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",path);
                }

           }

           }
          break;
          default:
          printf("Tipo de archivo invalido \n ");
          printf("Opciones validas: [b],[c],[d],[p],[f],[l],[s]\n");
           break;
     }
   }
   } else{
       if( flags[4] == 1  ) { // Busqueda por permisos
              if(strcmp(permisos,mode) == 0){
                if( flags[5] == 0){
                  if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],path);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,path,(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",path);
                }
                }else{
                  pwd = getpwuid(archivo.st_uid);
                  if(strcmp(pwd->pw_name,duenio) == 0){
                    if(flags[3] == 1){
                  if(argumentos == 1){
                    printf("-----------------------------\n");
                    if(fork() == 0){
                      strcpy(argv2[1],path);
                      argv2[elementos+2] = "0";
                      valid =  execvp(execpapu,argv2);
                      if(valid == -1){
                       printf("Error comando Invalido\n");
                      }

                    }else{
                      wait(&e);
                    }

                  }else{
                    if(fork() == 0){
                      //printf("Ejecutando:%s\n",execpapu);
                      valid =  execlp(execpapu,execpapu,path,(char *)0);
                      if(valid == -1){
                        printf("Error comando Invalido\n");
                      }
                    }else{
                      wait(&e);
                    }

                  }

                }else{
                  printf("%s\n",path);
                }
                  }

                }
              }

       }else{
         if( flags[5] == 1){ // Busqueda por duenio
           pwd = getpwuid(archivo.st_uid);
           if( strcmp(pwd->pw_name,duenio) == 0 ){
             printf("%s\n",path);
           }
           // Termina los criterios de Busqueda

         }
         else{
           printf("Error: No Hay Criterios De Busqueda :( \n");
           break;
         }
       }

   }
         }
        }
    }
  }
  closedir(actual);
  return 0;
}
int main(int argc, char **argv){
    // argv contine los argumentos del llamado a find
    int ruta_absoluta = 0; // Si es 0 esta encendida
    char* ruta;
    DIR *dir; // Apuntador al directorio
    struct dirent *entrada; // Guarda el directorio
    struct stat archivo;
    ruta = argv[1];
    if(strcmp(argv[1],".") == 0){
        ruta_absoluta = 1;
    }
    // Realiza la busqueda
    readVariable(argv);
    OpenDir(ruta,ruta_absoluta);// Abre el directorio
    if(contador > 0 && flags[0] == 1 && flags[2] == 1 ) {
      filtrarResultados(resultados);
    }else{
      if(contador > 0 && flags[4] == 1 && flags[5] == 0){
        filtroPermisos(resultados,contador);
      }else{
        if( contador > 0 && flags[4] == 0 && flags[5] == 1 && flags[0] == 1){
          filtroDuenio(resultados,contador);
        }else{
          if( contador > 0 && flags[2] == 1 && flags[5] == 1 && flags[4] == 1){
            filtrarResultados(resultados);
          }
        }
      }

    }


}






























